from django.contrib import admin
from django.urls import path, include
from swagger.urls import urlpatterns as doc_url


urlpatterns = [
    path('admin/', admin.site.urls),
    path("api/v1/", include("src.urls"))
]

urlpatterns += doc_url
