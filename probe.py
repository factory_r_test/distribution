import requests

headers = {
            "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzU4NDcyODUsImlzcyI6ImZhYnJpcXV"
                             "lIiwibmFtZSI6ImRtaXRyeS5vbHNoYW5uaWtvdiJ9.gtsEJ0MIsEfnbf6X0HDngF6ExgSvBlTZnge_-ugII8E"
        }
try:
    response = requests.post(
                url="https://probe.fbrq.cloud/v1/send/111111",
                json={
                    "id": 111111,
                    "phone": 79666666666,
                    "text": "text"
                },
                headers=headers,
                timeout=60
            )
    print(response.status_code)
    print(response.json())
    print(response.content)
except Exception as ex:
    print(ex)
