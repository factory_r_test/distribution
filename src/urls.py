from django.urls import path, include


urlpatterns = [
    path("client/", include("src.client.urls")),
    path("distribution/", include("src.distribution.urls")),
    path("message/", include("src.message.urls")),
]
