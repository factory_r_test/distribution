from rest_framework import serializers

from src.message.models import Message


class MessageDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Message
        fields = "__all__"
