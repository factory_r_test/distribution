from django.db import models
from src.distribution.models import Distribution
from src.client.models import Client


class Message(models.Model):
    STATUS_CHOICES = (
        ('s', "send"),
        ('n', 'no_send'),
    )

    create_data = models.DateTimeField(auto_created=True, auto_now_add=True, verbose_name="Дата создания")
    status = models.CharField(max_length=1, choices=STATUS_CHOICES, verbose_name="Статус")
    distribution = models.ForeignKey(
        to=Distribution, verbose_name="Рассылка", related_name="messages", on_delete=models.CASCADE
    )
    client = models.ForeignKey(
        to=Client, verbose_name="Клиент", related_name="messages", on_delete=models.CASCADE
    )
