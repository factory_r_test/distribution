import re


def validation_phone(value):
    pattern = re.compile("^79\d{9}$")
    values_list = re.findall(pattern, str(value))

    return values_list

def validation_time_zone(value):
    if -12 <= value <= 14:
        return True
