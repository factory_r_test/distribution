from rest_framework.exceptions import APIException
from rest_framework import status


class CustomException(APIException):

    def __init__(self, detail: str, status_code=None):

        if status_code is None:
            status_code = status.HTTP_400_BAD_REQUEST

        self.status_code = status_code


        self.detail = {
            "success": False,
            "error": detail,
        }
