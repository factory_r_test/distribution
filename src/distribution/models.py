from django.db import models


class Distribution(models.Model):
    start_date_time = models.DateTimeField(verbose_name="Время старта рассылки")
    message = models.TextField(verbose_name="Текст рассылки")
    end_date_time = models.DateTimeField(verbose_name="Время окончания рассылки")
    is_active = models.BooleanField(default=False)

    def __str__(self):
        return f"Рассылка сообщения {self.message[:20]}... - начало: {self.start_date_time} конец: {self.end_date_time}"


class DistributionFilter(models.Model):
    phone = models.CharField(verbose_name="Номер телефона", max_length=11, blank=True, null=True)
    tag_name = models.CharField(verbose_name="Имя тега", max_length=255, blank=True, null=True)
    distribution = models.ForeignKey(
        verbose_name="Рассылка", to=Distribution, on_delete=models.CASCADE, related_name="filters"
    )

    def __str__(self):
        if self.tag_name:
            return f"Фильтр тега - {self.tag_name} - рассылки {self.distribution}"
        else:
            return f"Фильтр телефона - {self.phone} - рассылки {self.distribution}"
