from django.urls import path, include

from src.distribution.api import CreateDistributionAPIView, DistributionAPIView, DistributionUpdateAPIView, \
    MessageDistributionAPIView, StatisticDistributionsAPIView

urlpatterns = [
    path("create", CreateDistributionAPIView.as_view()),
    path("detail/<int:pk>", DistributionAPIView.as_view()),
    path("update/<int:pk>", DistributionUpdateAPIView.as_view()),
    path("messages/<int:pk>", MessageDistributionAPIView.as_view()),
    path("statistic", StatisticDistributionsAPIView.as_view()),
]
