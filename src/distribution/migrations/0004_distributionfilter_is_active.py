# Generated by Django 4.0.2 on 2022-02-08 23:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('distribution', '0003_distributionfilter_distribution_and_more'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributionfilter',
            name='is_active',
            field=models.BooleanField(default=1),
            preserve_default=False,
        ),
    ]
