# Generated by Django 4.0.2 on 2022-02-08 21:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('distribution', '0002_alter_distributionfilter_phone'),
    ]

    operations = [
        migrations.AddField(
            model_name='distributionfilter',
            name='distribution',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, related_name='filters', to='distribution.distribution', verbose_name='Рассылка'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='distribution',
            name='message',
            field=models.TextField(verbose_name='Текст рассылки'),
        ),
        migrations.AlterField(
            model_name='distributionfilter',
            name='tag_name',
            field=models.CharField(blank=True, max_length=255, null=True, verbose_name='Имя тега'),
        ),
    ]
