import time
from datetime import datetime
from threading import Thread

import requests
from django.utils import timezone

from src.client.models import Client, ClientTag
from src.distribution.models import Distribution

from src.message.models import Message


class DistributionMakerThread(Thread):
    def __init__(self, distribution: Distribution):
        super().__init__()
        self.distribution = distribution
        self.headers = {
            "content-type": "application/json",
            "accept": "application/json",
            "Authorization": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2NzU4NDcyODUsImlzcyI6ImZhYnJpcXV"
                             "lIiwibmFtZSI6ImRtaXRyeS5vbHNoYW5uaWtvdiJ9.gtsEJ0MIsEfnbf6X0HDngF6ExgSvBlTZnge_-ugII8E"
        }
        self.client_list = []
        self.bad_messages = []

    def run(self):
        self.complete_client_list()

        if self.client_list:
            for client in self.client_list:
                if self.distribution.end_date_time < datetime.now(tz=timezone.utc):
                    return False
                self.send_message(client)

        while self.bad_messages:
            self.bad_messages = []
            for bad_message in self.bad_messages:
                self.send_message(client=bad_message.client, message=bad_message)

    def send_message(self, client: Client, message: Message = None):
        """Отправляет сообщение, если не передано сообщение, то оно будет создано"""
        if not message:
            message = Message.objects.create(status="n", distribution=self.distribution, client=client)
        try:
            response = requests.post(
                url=f"https://probe.fbrq.cloud/v1/send/{message.id}",
                json={
                    "id": message.id,
                    "phone": message.client.phone,
                    "text": message.distribution.message
                },
                headers=self.headers,
                timeout=60
            )
            time.sleep(3)
            if response.status_code == 200:
                if response.json().get("message") == "OK":
                    message.status = "s"
                    message.save()
                    return True
            self.bad_messages.append(message)
            time.sleep(2)
        except Exception as ex:
            time.sleep(20)
            pass

    def complete_client_list(self):
        """Формирует лист клиентов по фильтрам рассылки"""
        filters = self.distribution.filters.all()

        for filter in filters:
            if filter.phone:
                client = Client.objects.filter(phone=filter.phone)
                if client.exists():
                    if client not in self.client_list:
                        self.client_list.append(client[0])
            else:
                clients_of_tag = ClientTag.objects.filter(tag__name=filter.tag_name)
                if clients_of_tag.exists():
                    for client_of_tag in clients_of_tag:
                        if client_of_tag.client not in self.client_list:
                            self.client_list.append(client_of_tag.client)

