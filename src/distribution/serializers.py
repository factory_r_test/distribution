from rest_framework import serializers

from src.base import validation
from src.client.models import Tag
from src.distribution.models import Distribution, DistributionFilter
from src.message.serializers import MessageDetailSerializer


class DistributionCreateSerializer(serializers.ModelSerializer):
    filters_tag_name = serializers.ListField(child=serializers.CharField(), required=False)
    filters_phone = serializers.ListField(child=serializers.IntegerField(), required=False)

    class Meta:
        model = Distribution
        fields = "__all__"

    def validate_filters_phone(self, value):
        for phone in value:
            if not validation.validation_phone(phone):
                raise serializers.ValidationError(f"Не корректный номер телефона - {phone}")
        return value

    def validate_filters_tag_name(self, value):
        for tag_name in value:
            tag = Tag.objects.filter(name=tag_name.capitalize())
            if not tag.exists():
                raise serializers.ValidationError(f"Тега с name - {tag_name} не существует")
        return value

    def validate(self, values):
        start_date_time = values.get("start_date_time")
        end_date_time = values.get("end_date_time")

        if end_date_time <= start_date_time:
            raise serializers.ValidationError(f"Время начала не может быть позже или равно времени конца")

        return values

    def create(self, validated_data):
        filters_tag_name = []
        filters_phone = []

        if validated_data.get("filters_tag_name"):
            filters_tag_name = validated_data.pop("filters_tag_name")

        if validated_data.get("filters_phone"):
            filters_phone = validated_data.pop("filters_phone")
        distribution = Distribution.objects.create(**validated_data)

        for filter_tag_name in filters_tag_name:
            DistributionFilter.objects.create(tag_name=filter_tag_name.capitalize(), distribution=distribution)

        for filter_phone in filters_phone:
            DistributionFilter.objects.create(phone=filter_phone, distribution=distribution)

        return distribution


class DistributionFilterDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = DistributionFilter
        fields = "__all__"


class DistributionDetailSerializer(serializers.ModelSerializer):
    filters = DistributionFilterDetailSerializer(many=True)

    class Meta:
        model = Distribution
        fields = "__all__"


class DistributionUpdateSerializer(serializers.ModelSerializer):
    filters_tag_name = serializers.ListField(child=serializers.CharField(), required=False, allow_empty=True)
    filters_phone = serializers.ListField(child=serializers.IntegerField(), required=False, allow_empty=True)

    class Meta:
        model = Distribution
        fields = "__all__"

    def validate_filters_phone(self, value):
        for phone in value:
            if not validation.validation_phone(phone):
                raise serializers.ValidationError(f"Не корректный номер телефона - {phone}")
        return value

    def validate_filters_tag_name(self, value):
        for tag_name in value:
            tag = Tag.objects.filter(name=tag_name.capitalize())
            if not tag.exists():
                raise serializers.ValidationError(f"Тега с name - {tag_name} не существует")
        return value

    def validate(self, values):
        start_date_time = values.get("start_date_time")
        end_date_time = values.get("end_date_time")

        if end_date_time <= start_date_time:
            raise serializers.ValidationError(f"Время начала не может быть позже или равно времени конца")

        return values

    def update(self, instance, validated_data):
        instance.start_date_time = validated_data.get("start_date_time", instance.start_date_time)
        instance.message = validated_data.get("message", instance.message)
        instance.end_date_time = validated_data.get("end_date_time", instance.end_date_time)
        instance.save()
        filters_tag_name = None
        filters_phone = None

        if validated_data.get("filters_tag_name"):
            filters_tag_name = validated_data.pop("filters_tag_name")

        if validated_data.get("filters_phone"):
            filters_phone = validated_data.pop("filters_phone")

        if filters_tag_name is not None or filters_phone is not None:
            instance.filters.all().delete()

            if isinstance(filters_phone, list):
                for filter_phone in filters_phone:
                    DistributionFilter.objects.create(distribution=instance, phone=filter_phone)

            if isinstance(filters_tag_name, list):
                for filter_tag_name in filters_tag_name:
                    DistributionFilter.objects.create(distribution=instance, tag_name=filter_tag_name.capitalize())

        return instance


class DistributionMessageListDetailSerializer(serializers.ModelSerializer):
    messages = MessageDetailSerializer(many=True)

    class Meta:
        model = Distribution
        fields = ("messages",)
