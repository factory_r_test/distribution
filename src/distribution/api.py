from rest_framework import generics
from rest_framework.views import APIView
from rest_framework.response import Response

from src.base.exceptions import CustomException
from src.distribution.models import Distribution
from src.distribution.serializers import DistributionCreateSerializer, DistributionDetailSerializer, \
    DistributionUpdateSerializer, DistributionMessageListDetailSerializer
from src.message.models import Message


class CreateDistributionAPIView(generics.CreateAPIView):
    serializer_class = DistributionCreateSerializer


class DistributionAPIView(generics.RetrieveDestroyAPIView):
    serializer_class = DistributionDetailSerializer
    queryset = Distribution.objects.all()


class DistributionUpdateAPIView(generics.GenericAPIView):
    serializer_class = DistributionUpdateSerializer

    def put(self, request, pk):
        serializer = self.serializer_class(data=request.data, partial=True)

        if serializer.is_valid(raise_exception=True):
            distribution = Distribution.objects.filter(id=pk)

            if not distribution:
                raise CustomException("Нет рассылки с таким id")

            distribution = distribution.first()

            serializer.update(instance=distribution, validated_data=serializer.validated_data)

            serializer = DistributionDetailSerializer(distribution)

            return Response(serializer.data)


class StatisticDistributionsAPIView(APIView):

    def get(self, request):
        data = []

        distributions = Distribution.objects.all()

        for distribution in distributions:
            count_s_message = Message.objects.filter(distribution=distribution, status="s").count()
            count_n_message = Message.objects.filter(distribution=distribution, status="n").count()

            data.append({"distribution_id": distribution.id, "send": count_s_message, "no_send": count_n_message})

        return Response(data)


class MessageDistributionAPIView(generics.RetrieveAPIView):
    serializer_class = DistributionMessageListDetailSerializer
    queryset = Distribution.objects.all()
