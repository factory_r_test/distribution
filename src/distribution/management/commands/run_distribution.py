from django.core.management.base import BaseCommand
from django.utils import timezone
from datetime import datetime


from src.distribution.distribution_client import DistributionMakerThread
from src.distribution.models import Distribution


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        distributions = Distribution.objects.filter(
            is_active=False,
            start_date_time__lt=datetime.now(tz=timezone.utc),
            end_date_time__gt=datetime.now(tz=timezone.utc)
        )

        if distributions:
            for distribution in distributions:
                distribution.is_active = True
                distribution.save()

        threads = []
        for distribution in distributions:
            threads.append(DistributionMakerThread(distribution=distribution))

        for thread in threads:
            thread.start()

        for thread in threads:
            thread.join()
