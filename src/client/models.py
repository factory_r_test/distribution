from django.db import models


class Client(models.Model):
    phone = models.CharField(max_length=11, verbose_name="Номер телефона", unique=True)
    phone_code = models.CharField(max_length=10, verbose_name="Код мобильного оператора")
    time_zone = models.IntegerField(verbose_name="Временной пояс utc")

    def __str__(self):
        return f'Клиент - {self.id}'


class Tag(models.Model):
    name = models.CharField(max_length=255, verbose_name="Название тега", unique=True)

    def __str__(self):
        return self.name


class ClientTag(models.Model):
    client = models.ForeignKey(to=Client, verbose_name="Клиент тега", related_name="tags", on_delete=models.CASCADE)
    tag = models.ForeignKey(to=Tag, verbose_name="Тег клиента", related_name="clients", on_delete=models.CASCADE)

    def __str__(self):
        return f"Клиент {self.client.id} имеет тег - {self.tag.id}"
