from rest_framework import generics
from rest_framework.response import Response

from src.base.exceptions import CustomException
from src.client.models import Client, Tag, ClientTag
from src.client.serializers import CreateClientSerializer, TagCreateSerializer, ClientUpdateSerializer, \
    ClientDetailSerializer


class CreateClientAPIView(generics.CreateAPIView):
    queryset = Client.objects.all()
    serializer_class = CreateClientSerializer


class TagCreateAPIView(generics.CreateAPIView):
    serializer_class = TagCreateSerializer


class ClientAPIView(generics.RetrieveDestroyAPIView):
    serializer_class = ClientDetailSerializer
    queryset = Client.objects.all()


class ClientUpdateAPIView(generics.GenericAPIView):
    serializer_class = ClientUpdateSerializer
    queryset = Client.objects.all()

    def put(self, request, pk):
        serializer = self.serializer_class(data=request.data, partial=True)

        if serializer.is_valid(raise_exception=True):
            client = Client.objects.filter(id=pk)
            if not client:
                raise CustomException("Нет клиента с таким id")
            client = client.first()

            serializer.update(instance=client, validated_data=serializer.validated_data)

            serializer = ClientDetailSerializer(client)

            return Response(serializer.data)
