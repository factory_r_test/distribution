from rest_framework import serializers

from src.client.models import Client, Tag, ClientTag
from src.base import validation


class CreateClientSerializer(serializers.ModelSerializer):
    tag_id_list = serializers.ListField(child=serializers.IntegerField(), required=False)


    class Meta:
        model = Client
        fields = "__all__"

    def validate_tag_id_list(self, value):
        for tag_id in value:
            tag = Tag.objects.filter(id=tag_id)
            if not tag.exists():
                raise serializers.ValidationError(f"Тега с id - {tag_id} не существует")
        return value

    def validate_phone(self, value):
        if not validation.validation_phone(value):
            raise serializers.ValidationError("Не корректный номер телефона")
        return value

    def validate_time_zone(self, value):
        if not validation.validation_time_zone(value):
            raise serializers.ValidationError("Не корректная временная зона")
        return value

    def create(self, validated_data):
        tag_id_list = []
        if validated_data.get("tag_id_list"):
            tag_id_list = validated_data.pop("tag_id_list")

        client = Client.objects.create(**validated_data)
        for tag_id in tag_id_list:
            tag = Tag.objects.get(id=tag_id)
            ClientTag.objects.create(client=client, tag=tag)

        return client


class TagCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = "__all__"

    def validate_name(self, value: str):
        value = value.capitalize()

        if Tag.objects.filter(name=value).exists():
            raise serializers.ValidationError("Уже есть тег с таким именем")
        return value.capitalize()


class TagDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = "__all__"


class ClientTagDetailSerializer(serializers.ModelSerializer):
    tag = TagDetailSerializer()

    class Meta:
        model = ClientTag
        fields = ("tag", "client")


class ClientDetailSerializer(serializers.ModelSerializer):
    tags = ClientTagDetailSerializer(many=True)

    class Meta:
        model = Client
        fields = "__all__"


class ClientUpdateSerializer(serializers.ModelSerializer):
    tag_id_list = serializers.ListField(child=serializers.IntegerField(), required=False, allow_empty=True)

    class Meta:
        model = Client
        fields = "__all__"

    def validate_tag_id_list(self, value):
        for tag_id in value:
            tag = Tag.objects.filter(id=tag_id)
            if not tag.exists():
                raise serializers.ValidationError(f"Тега с id - {tag_id} не существует")
        return value

    def validate_phone(self, value):
        if not validation.validation_phone(value):
            raise serializers.ValidationError("Не корректный номер телефона")
        return value

    def validate_time_zone(self, value):
        if not validation.validation_time_zone(value):
            raise serializers.ValidationError("Не корректная временная зона")
        return value

    def update(self, instance, validated_data):
        instance.phone = validated_data.get("phone", instance.phone)
        instance.phone_code = validated_data.get("phone_code", instance.phone_code)
        instance.time_zone = validated_data.get("time_zone", instance.time_zone)
        instance.save()

        tag_id_list = validated_data.get("tag_id_list")

        if tag_id_list is not None:
            instance.tags.all().delete()

            for tag_id in tag_id_list:
                tag = Tag.objects.get(id=tag_id)
                ClientTag.objects.create(client=instance, tag=tag)

        return instance
