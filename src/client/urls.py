from django.urls import path

from src.client.api import CreateClientAPIView, TagCreateAPIView, ClientAPIView, ClientUpdateAPIView

urlpatterns = [
    path("create", CreateClientAPIView.as_view()),
    path("tag/create", TagCreateAPIView.as_view()),
    path("detail/<int:pk>", ClientAPIView.as_view()),
    path("update/<int:pk>", ClientUpdateAPIView.as_view())
]
