from django.urls import path
from drf_spectacular.views import SpectacularAPIView, SpectacularRedocView, SpectacularSwaggerView


urlpatterns = {
    path("schema", SpectacularAPIView.as_view(), name="schema"),
    path('swagger-ui', SpectacularSwaggerView.as_view(url_name='schema')),
    path('redoc', SpectacularRedocView.as_view(url_name='schema')),
}

def preprocessing_filter_spec(endpoints):
    filtered = []
    for (path, path_regex, method, callback) in endpoints:
        # Remove all but DRF API endpoints
        if path.startswith("/api/"):
            filtered.append((path, path_regex, method, callback))
    return filtered
