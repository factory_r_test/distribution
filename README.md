Для запуска проекта необходимо.
1. Склонировать репозиторий
2. conf уже создан, не чего менять не нужно
3. Создайте виртуальное окружение
3. Установить нужные пакеты pip install -r requirements.txt
Из за нехватки времени для простоты работы рассылка работает через cron
В файле crontab пропишите строку
* * * * * user путь до виртуального окружения файла python путь до manage.py run_distribution

Список выполняемый запросов.

Создание клиента post /api/v1/client/create
Создание тега post /api/v1/client/tag/create
Удаление клиента delete /api/v1/client/detail/{id}
Получение данных клиента get /api/v1/client/detail/{id}
Обновление данных клиента put /api/v1/client/update/{id}

Создание рассылки post /api/v1/distribution/create
Удаление рассылки delete /api/v1/distribution/detail/{id}
Получение данных рассылки get /api/v1/distribution/detail/{id}
Обновление данных рассылки put /api/v1/distribution/update/{id}
Получение сообщений по рассылке get /api/v1/distribution/messages/{id}
Получение статистики по всем рассылкам get /api/v1/distribution/statistic

Запустить команду на отправку сообщений по рассылкам python manage.py run_distribution
